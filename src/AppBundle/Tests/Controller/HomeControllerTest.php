<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', 'http://127.0.0.1:8000/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('File Search', $crawler->filter('#container h1')->text());
        $this->assertContains('Enter Search Term', $crawler->filter('label')->text());
        $this->assertContains('Search', $crawler->filter('button')->text());
    }
}
