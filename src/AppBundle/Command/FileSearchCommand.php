<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FileSearchCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('file:search')
                ->setDescription('Search FileSearch DB from shell')
                ->addArgument('searchterm', InputArgument::REQUIRED, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $search_term = $input->getArgument('searchterm');


        if (!$search_term) {
            $output->writeln('Please enter search term');
            return;
        }
        $data = $this->getContainer()->get('filesearch')->getSearchFromEngine($search_term);
        if (count($data)) {
            $mask = "|%5.5s |%-20.20s |%-50.50s  |\n";
            printf($mask, 'ID', 'Filename', 'Content');
            foreach ($data as $result) {
                printf($mask, $result['id'], $result['filename'], str_replace(array("\r", "\n"), '', $result['content']));
            }
            return;
        }
        $output->writeln('No results');
    }

}
