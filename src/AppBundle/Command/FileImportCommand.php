<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Content;

class FileImportCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('file:import')
                ->setDescription('DB file importer')
                ->addArgument('filename', InputArgument::REQUIRED, 'Plaese specify filename')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $file = $input->getArgument('filename');

        if (!file_exists($file) || mime_content_type($file) != "text/plain") {
            $output->writeln('Invalid file or file doesn\'t exists');
            return;
        }

        $file_content = file_get_contents($file);
        $file_info = pathinfo($file);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $content = new Content();

        try {
            $content->setContent($file_content);
            $content->setFilename(basename($file)); // no record of directory for this test
            $em->persist($content);
            $em->flush();

            $output->writeln('File ' . $file . ' imported to DB');
        } catch (Exception $e) {
            $output->writeln($e->getMessage());
        }
    }

}
