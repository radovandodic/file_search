<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
//use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchApiController extends Controller {

    /**
     * POST Route annotation.
     * @var ParamFetcher $paramFetcher
     * @RequestParam(name="search", description="search")
     * @Post("/api/v1/search")
     * @View()
     * @return array
     */
    public function cpostAction(ParamFetcher $paramFetcher) {
        if ($search_term = $paramFetcher->get('search')) {
            $data = $this->get('filesearch')->getSearchFromEngine($search_term);
        }
        return (isset($data) && !empty($data)) ? $data : array('data' => 'Search result is empty');
    }

    /**
     * GET Route annotation.
     * @Get("/api/v1/search")
     */
    public function cgetAction() {
        return $this->redirectToRoute('homepage');
    }

}
