<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Content;

class SearchController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        $form = $this->getForm($request);

        if ($this->checkForm($form, $request)) {
            return $this->redirectToRoute('search');
        }

        return $this->render('AppBundle:default:index.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/search", name="searchpage")
     */
    public function searchAction(Request $request) {
        $form = $this->getForm($request);
        if ($this->checkForm($form, $request)) {
            $search_term = $request->request->get('form[search]', null, true);
            if (!$search_term)
                break;
            $entities = $this->get('filesearch')->getSearchFromEngine($search_term);
            return $this->render('AppBundle:default:search.html.twig', array(
                        'results' => $entities, 'form' => $form->createView(),
            ));
        }
        return $this->redirectToRoute('homepage');
    }

    private function getForm($request) {
        $form = $this->createFormBuilder()
                ->setAction($this->get('router')->generate('searchpage'))
                ->add('search', TextType::class, array('label' => 'Enter Search Term'))
                ->add('submit', SubmitType::class, array('label' => 'Search'))
                ->getForm();
        $form->handleRequest($request);
        return $form;
    }

    private function checkForm($form, Request $request) {
        return (bool) ($form->isSubmitted() && $form->isValid() && $request->getMethod() == 'POST');
    }

}
