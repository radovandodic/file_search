<?php

namespace AppBundle\SearchManager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DoctrineSearchManager extends Controller {

    protected $em;

    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
    }

    public function getEntities($search_term) {
        return $this->em->getRepository('AppBundle:Content')->getEntities($search_term);
    }

}
