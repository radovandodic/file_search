<?php

namespace AppBundle\SearchManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SearchManager extends Controller {
    
    /**
    * @param ContainerInterface $container
    */
   public function __construct($engine, ContainerInterface $container)
    {
        $this->container = $container;
        $this->engine = $engine;
    }
    
    public function getSearchFromEngine($search_term) {
        return $this->get('file'.$this->engine.'search')->getEntities($search_term);
    }
    
}