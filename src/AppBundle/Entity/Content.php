<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Content
 *
 * @ORM\Table(name="search_content", indexes={@ORM\Index(columns={"content"}, flags={"fulltext"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContentRepository")
 */
class Content {


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @var text
     *
     * @ORM\Column(name="content", type="text", length=16777215)
     */
    private $content;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param $content
     *
     * @return Content
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return \MEDIUMTEXT
     */
    public function getContent() {
        return $this->content;
    }


    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Content
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }
}
